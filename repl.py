from lisp import evaluate, getvarlist
from parse import tree

while True:
    line = input('lisp>>')
    if line == 'var_list()':
        print(getvarlist())
        continue

    expr = [x for x in line.replace('(', ' ( ').replace(')', ' ) ').split(' ') if x != '']
    syntax_tree = tree(expr)
    print(evaluate(syntax_tree))
    #print(syntax_tree)