import re

def deffered_input(string):
    val = input(string)
    if re.match(r'^(\+|-)?\d+$', val):
        return int(val)
    elif re.match(r'^[-+]?\d*\.?\d*$', val):
        return float(val)
    else:
        return val

def isstring(string):
    if string[0] == "'" and string[-1] == "'":
        return True
    return False