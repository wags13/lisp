import operator as op
from functools import reduce
import re
import util

class Function:
    def __init__(self, n, func):
        self._n = n
        self._func = func

    def getn(self):
        return self._n

    def getfn(self):
        return self._func

def cbool(boolean):
    if boolean == True:
        return 1
    return 0

def define(var, val, var_list):
    if var in var_list.keys():
        raise Exception
    if var in funcs.keys():
        raise Exception
    if isinstance(var, int):
        raise Exception
    print(var)
    if not re.match(r'[_a-zA-Z][_a-zA-Z0-9]*', var):
        raise Exception
    else:
        var_list[var] = val
        return val

def assignment(var, val, var_list):
    if var not in var_list.keys():
        raise Exception
    if var in funcs.keys():
        raise Exception
    var_list[var] = val
    return val

def chain(*args):
    return [x for x in args]

def app(a, b):
    b.append(a)
    return b

def conditional(*args):
    if args[0]:
        return args[1]
    else:
        return 0
def conditional_else(*args):
    if args[0]:
        return args[1]
    else:
        return args[2]

def loop(fn, expr, *args):
    if not isinstance(args[0], (int, float)):
        raise Exception
    else:
        for _ in range(args[0] - 1):
            fn(expr)

def read(var=0, var_list=0):
    data = util.deffered_input('in>')
    if var == 0:
        return data
    else:
        return assignment(var, data, var_list)


funcs = {
    '+': Function(-1, lambda *args: sum(args)),
    '-': Function(2, op.sub),
    '*': Function(-1, lambda *args: reduce(op.mul, args)),
    '/': Function(2, op.truediv),
    '//': Function(2, op.floordiv),
    '**': Function(2, op.pow),
    '%': Function(2, op.mod),
    '>': Function(2, lambda a, b: cbool(a > b)),
    '>=': Function(2, lambda a, b: cbool(a >= b)),
    '<': Function(2, lambda a, b: cbool(a < b)),
    '<=': Function(2, lambda a, b: cbool(a <= b)),
    '==': Function(2, lambda a, b: cbool(a == b)),
    '!=': Function(2, lambda a, b: cbool(a != b)),
    '&&': Function(2, lambda a, b: cbool(a and b)),
    '||': Function(2, lambda a, b: cbool(a or b)),
    '!': Function(1, lambda a: cbool(not a)),
    '~': Function(1, lambda a: -1 * a),
    'def': Function(2, define),
    'zip': Function(-1, lambda *args: list(args)),
    '=': Function(2, assignment),
    '[]': Function(2, lambda a, b: a[b]),
    'append': Function(2, app),
    'pop': Function(1, lambda a: a.pop()),
    'if': Function(-1, (conditional, conditional_else)),
    'loop': Function(2, loop),
    'read': Function(-1, read)
}